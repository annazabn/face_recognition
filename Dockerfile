FROM python:3.7

WORKDIR /usr/src/cv_face_recognition
ENV PYTHONPATH .

RUN apt-get update

COPY requirements.txt ./
RUN pip install -r requirements.txt && rm requirements.txt
COPY . .

ENTRYPOINT ["bash", "entrypoint.sh"]
