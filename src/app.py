import os

from flask import Flask

from src.config import CONFIG as CFG
from src.utils.router import Router

app_cfg = CFG['APP']
port = os.getenv('PORT', app_cfg['PORT'])
app = Router().apply_routes(Flask(app_cfg['NAME']))

if __name__ == '__main__':
    app.run(port=port)
