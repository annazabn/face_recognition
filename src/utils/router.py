from src.http_handlers import UploadsHTTPHandler, MatchHTTPHandler, CompareHTTPHandler, \
    APIKeysHTTPHandler, Healthcheck


class Router:

    @staticmethod
    def apply_routes(app):
        app.add_url_rule('/facerecognition/api/v1/upload', 'upload',
                         view_func=UploadsHTTPHandler.as_view('upload'),
                         methods=['GET', 'POST', 'DELETE'])
        app.add_url_rule('/facerecognition/api/v1/match', 'match',
                         view_func=MatchHTTPHandler.as_view('match'),
                         methods=['POST'])
        app.add_url_rule('/facerecognition/api/v1/compare', 'compare',
                         view_func=CompareHTTPHandler.as_view('compare'),
                         methods=['POST'])
        app.add_url_rule('/facerecognition/api/v1/tokens', 'tokens',
                         view_func=APIKeysHTTPHandler.as_view('tokens'),
                         methods=['GET', 'PUT', 'DELETE'])
        app.add_url_rule('/healthy', 'health',
                         view_func=Healthcheck.as_view('health'),
                         methods=['GET'])
        return app
