import logging
import os


def singleton(class_):
    instances = {}

    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance


@singleton
class Logger:

    def __init__(self, name: str,
                 format: str = '%(filename)s - %(asctime)s - %(levelname)s - %(message)s'):

        log_dir = 'logs'
        log_file = 'app.log'
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)

        logging.basicConfig(filename=os.path.join(log_dir, log_file), filemode='a', format=format)
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.DEBUG)

    def get_logger(self) -> logging:
        return self.logger
