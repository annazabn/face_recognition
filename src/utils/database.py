import os
import random

from sortedcontainers import SortedDict
from pymongo import MongoClient

from src.utils import helper
from src.utils.helper import PC


class DBInterface:

    def __init__(self, host, port):
        client = MongoClient(host, port)
        db = client.face_db
        self.__photos = db.photos
        self.__userdata = db.userdata

    def save_image(self, path, tag, embeddings):
        best_embedding = helper.get_best_embedding(embeddings)
        self.__photos.insert_one({"path": path, "tag": str(tag),
                                "descriptor": best_embedding["descriptor"],
                                "points": best_embedding["points"]})
        return best_embedding["points"]

    def search_matches(self, embeddings, answer_size):
        descriptor = helper.get_best_embedding(embeddings)["descriptor"]
        sd = SortedDict()
        for photo in self.__photos.find():
            dis = PC.euklid_distance(photo["descriptor"], descriptor)
            data = {"path": photo["path"], "tag": photo["tag"],
                    "proximity": PC.proximity(dis)}
            sd.update({dis: data})
            if len(sd) > answer_size:
                sd.popitem(answer_size)
        return sd

    def count(self, tag=None):
        if tag is not None:
            return self.__photos.count({'tag': tag})
        else:
            return self.__photos.count()

    def delete(self, name, tag):
        if name == 'all':
            for item in self.__photos.find({}, {'path': 1, '_id': 0}):
                if os.path.exists(item['path']):
                    os.remove(item['path'])
            removed = self.__photos.count()
            self.__photos.remove({})
            return removed

        removed = 0

        if name is not None:
            save_folder = os.getenv('UPLOAD_FOLDER', '/usr/src/uploads/')
            path = os.path.join(save_folder, name)
            removed += self.__photos.count({'path': path})
            self.__photos.remove({'path': path})
            os.remove(path)

        if tag is not None:
            for item in self.__photos.find({'tag': tag}, {'path': 1, '_id': 0}):
                if os.path.exists(item['path']):
                    os.remove(item['path'])
            removed += self.__photos.count({'tag': tag})
            self.__photos.remove({'tag': tag})

        return removed

    def validate_api_key(self, key):
        return self.__userdata.find_one({'api_key': key}) is not None

    def create_api_key(self):
        new_key = self.__generate_key()
        while self.validate_api_key(new_key):
            new_key = self.__generate_key()
        self.__userdata.insert_one({'api_key': new_key})
        return new_key

    def get_api_keys(self):
        keys = []
        for data in self.__userdata.find():
            keys.append(data['api_key'])
        return keys

    def delete_api_key(self, key):
        return self.__userdata.remove({'api_key': key})['n'] > 0

    def __generate_key(self):
        return ''.join([chr(random.randint(*random.choice([(48, 57), (97, 122)])))
                        for _ in range(34)])


DB = DBInterface(os.getenv("MONGO_HOST", 'localhost'), int(os.getenv('MONGO_PORT', 27017)))
