import base64
import cv2
import io
import json
import numpy as np

from PIL import Image
from flask import Response

from src.config import CONFIG as CFG


paths = CFG['RESOURCES']


def json_response(status_code, message, result=None):
    msg = {"status_code": status_code, "message": message}
    if result is not None:
        msg.update(result)
    return Response(json.dumps(msg), status_code)


def byte_str_to_img(byte_str):
    try:
        b = base64.b64decode(byte_str)
    except base64.binascii.Error:
        return None
    try:
        in_img = Image.open(io.BytesIO(b))
    except OSError:
        return None
    return np.array(in_img)


def img_to_byte_str(path):
    with open(path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode('utf-8')


def get_best_embedding(embeddings):
    points = np.array([emb[1] for emb in embeddings])
    best_face_index = np.argmax([cv2.norm((p[0], p[1]), (p[2], p[3])) for p in points])
    return {"descriptor": embeddings[best_face_index][0].tolist(),
            "points": embeddings[best_face_index][1].tolist()[:4]}


class PropertiesCalculator:

    def __init__(self):
        self.threshold = np.sqrt(1.2)
        self.model_range = 1.8

    def euklid_distance(self, desc1, desc2):
        return np.sqrt(np.sum(np.square(np.subtract(desc1, desc2))))

    def proximity(self, distance):
        if distance > self.model_range:
            return 0
        else:
            model = np.load(paths['CONFIDENCE_MODEL']).item()
            return model(distance).item()

    def same_person(self, distance):
        return bool(distance <= self.threshold)


PC = PropertiesCalculator()
