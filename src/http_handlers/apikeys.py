from flask import request
from flask.views import MethodView

from src.utils import authorize, DB, utils


class APIKeysHTTPHandler(MethodView):

    @authorize
    def get(self):
        return utils.json_response(200, 'Success.', {"result": DB.get_api_keys()})

    @authorize
    def put(self):
        new_key = DB.create_api_key()
        return utils.json_response(200, 'Success.', {"result": new_key})

    @authorize
    def delete(self):
        body = request.get_json()
        key = body.get('token')

        if not isinstance(key, str):
            return utils.json_response(400, "Request must contain the 'token' string.")

        if DB.delete_api_key(key):
            return utils.json_response(200, 'Success.')
        else:
            return utils.json_response(400, 'The token does not exist.')
