import datetime
import hashlib
import json
import os

import cv2

from flask import request, Response
from flask.views import MethodView

from src.utils import DB, Logger, utils, authorize
from src.face_processor import get_embeddings


class UploadsHTTPHandler(MethodView):

    def __init__(self):
        self.logger = Logger('upload').get_logger()
        self.t_acc = 4

    @authorize
    def get(self):
        start_time = datetime.datetime.now()

        result = {'total_count': DB.count()}
        tag = request.args.get('countByTag')
        if tag is not None:
            result['tag_count'] = DB.count(tag=tag)

        elapsed_time = round((datetime.datetime.now() - start_time).total_seconds(), self.t_acc)
        self.logger.info(f'Counted photos in {elapsed_time} sec.')

        return utils.json_response(200, 'Success.', {'result': result})

    @authorize
    def post(self):
        start_time = datetime.datetime.now()

        body = request.get_json()
        images = body.get('images')

        if not isinstance(images, list):
            self.logger.warning(f'Error 400.')
            return utils.json_response(400, "Got request with no 'images' list.")
        if not len(images):
            self.logger.warning(f'Error 400.')
            return utils.json_response(400, 'Got request with empty images.')

        result = {'upload': [], 'added_image': 0}

        for i, image_data in enumerate(images):
            image_str = image_data.get('image')
            image_tag = image_data.get('tag', '<unspecified>')
            image_info = {'image_number': i + 1, 'image_tag': image_tag}

            if not isinstance(image_str, str):
                image_info.update({'status_code': 400, 'message': 'Empty or not a string image.'})
                result['upload'].append(image_info)
                continue

            image = utils.byte_str_to_img(image_str)
            if image is None:
                image_info.update({'status_code': 400, 'message': 'Unreadable image.'})
                result['upload'].append(image_info)
                continue

            image_data = get_embeddings(image)
            if image_data is None:
                image_info.update({'status_code': 400, 'message': 'No faces in the image.'})
                result['upload'].append(image_info)
                continue

            new_name = hashlib.sha256(image).hexdigest()
            path = self.__save_image(image, f'{new_name}.jpg')
            rect = DB.save_image(path, image_tag, image_data)
            image_info.update({'status_code': 200, 'message': 'Success.',
                               'faceRectangle': list(map(int, rect)), 'name': new_name})
            result['upload'].append(image_info)
            result['added_image'] += 1

        elapsed_time = round((datetime.datetime.now() - start_time).total_seconds(), self.t_acc)
        self.logger.info(f'Successfully uploaded {result["added_image"]} photos in '
                         f'{elapsed_time} sec.')

        return Response(json.dumps(result))

    @authorize
    def delete(self):
        start_time = datetime.datetime.now()

        body = request.get_json()
        name = body.get('name')
        tag = body.get('tag')

        if not isinstance(name, str) and not isinstance(tag, str):
            self.logger.warning(f'Error 400.')
            return utils.json_response(400, "Got request without the 'name' or the 'tag' string.")

        removed = DB.delete(name=name, tag=tag)

        elapsed_time = round((datetime.datetime.now() - start_time).total_seconds(), self.t_acc)
        self.logger.info(f'Successfully deleted {removed} photos in {elapsed_time} sec.')

        return utils.json_response(200, 'Success.', {'result': {'removed_total': removed}})

    def __save_image(self, img, name):
        save_folder = os.getenv('UPLOAD_FOLDER', '/usr/src/uploads/')
        if not os.path.exists(save_folder):
            os.makedirs(save_folder)
        path = os.path.join(save_folder, name)
        cv2.imwrite(path, cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        return path
