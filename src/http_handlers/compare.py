import datetime

from flask import request
from flask.views import MethodView

from src.utils import Logger, PC, utils, authorize
from src.face_processor import get_embeddings


class CompareHTTPHandler(MethodView):

    def __init__(self):
        self.logger = Logger('compare').get_logger()

    @authorize
    def post(self):
        t_acc = 4
        compare_start_time = datetime.datetime.now()

        body = request.get_json()
        image_str1 = body.get('image1')
        image_str2 = body.get('image2')

        if not isinstance(image_str1, str) and isinstance(image_str2, str):
            self.logger.warning(f'Error 400.')
            return utils.json_response(400, "Request must contain the 'image1' and 'image2' "
                                            "strings.")

        image1 = utils.byte_str_to_img(image_str1)
        image2 = utils.byte_str_to_img(image_str2)
        if image1 is None or image2 is None:
            self.logger.warning(f'Error 400.')
            return utils.json_response(400, 'Not all images can be read.')

        start_time = datetime.datetime.now()
        image_data1 = get_embeddings(image1)
        elapsed_time = round((datetime.datetime.now() - start_time).total_seconds(), t_acc)
        self.logger.info(f'Found faces in the first image in {elapsed_time} sec.')

        start_time = datetime.datetime.now()
        image_data2 = get_embeddings(image2)
        elapsed_time = round((datetime.datetime.now() - start_time).total_seconds(), t_acc)
        self.logger.info(f'Found faces in the second image in {elapsed_time} sec.')

        if image_data1 is None or image_data2 is None:
            self.logger.warning(f'Error 400.')
            return utils.json_response(400, 'Not all images contain faces.')

        start_time = datetime.datetime.now()
        best_embedding1 = utils.get_best_embedding(image_data1)
        elapsed_time = round((datetime.datetime.now() - start_time).total_seconds(), t_acc)
        self.logger.info(f'Found the best face in the first image in {elapsed_time} sec.')

        start_time = datetime.datetime.now()
        best_embedding2 = utils.get_best_embedding(image_data2)
        elapsed_time = round((datetime.datetime.now() - start_time).total_seconds(), t_acc)
        self.logger.info(f'Found the best face in the second image in {elapsed_time} sec.')

        start_time = datetime.datetime.now()
        distance = PC.euklid_distance(best_embedding1["descriptor"], best_embedding2["descriptor"])
        elapsed_time = round((datetime.datetime.now() - start_time).total_seconds(), t_acc)
        self.logger.info(f'Calculated distance in {elapsed_time} sec.')

        res = {"same_person": PC.same_person(distance),
               "distance": distance,
               "proximity": PC.proximity(distance),
               "faceRectangle1": list(map(int, best_embedding1["points"])),
               "faceRectangle2": list(map(int, best_embedding2["points"]))}

        elapsed_time = round((datetime.datetime.now() - compare_start_time).total_seconds(), t_acc)
        self.logger.info(f'Successfully compared in {elapsed_time} sec.')

        return utils.json_response(200, 'Success.', {"result": res})
