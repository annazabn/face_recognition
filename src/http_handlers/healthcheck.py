from flask.views import MethodView

from src.utils import utils


class Healthcheck(MethodView):

    def get(self):
        return utils.json_response(200, 'Success.')
