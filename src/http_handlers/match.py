import datetime

from flask import request
from flask.views import MethodView

from src.face_processor import get_embeddings
from src.utils import DB, Logger, PC, utils, authorize


class MatchHTTPHandler(MethodView):

    def __init__(self):
        self.logger = Logger('match').get_logger()

    @authorize
    def post(self):
        t_acc = 4
        match_start_time = datetime.datetime.now()

        body = request.get_json()
        image_str = body.get('image')
        answer_size = body.get('answer_size', 5)

        if not isinstance(image_str, str):
            self.logger.warning(f'Error 400.')
            return utils.json_response(400, "Request must contain the 'image' string.")
        if not isinstance(answer_size, int) or answer_size < 0:
            self.logger.warning(f'Error 400.')
            return utils.json_response(400, 'Answer size must be an integer greater than 0.')

        image = utils.byte_str_to_img(image_str)
        if image is None:
            self.logger.warning(f'Error 400.')
            return utils.json_response(400, 'The image cannot be read.')

        start_time = datetime.datetime.now()
        image_data = get_embeddings(image)
        elapsed_time = round((datetime.datetime.now() - start_time).total_seconds(), t_acc)
        self.logger.info(f'Found faces in the image in {elapsed_time} sec.')

        if image_data is None:
            self.logger.warning(f'Error 400.')
            return utils.json_response(400, 'No faces in the image.')

        start_time = datetime.datetime.now()
        face_matches = DB.search_matches(image_data, answer_size)
        person_matches = {k: v for k, v in face_matches.items() if PC.same_person(k)}
        elapsed_time = round((datetime.datetime.now() - start_time).total_seconds(), t_acc)
        self.logger.info(f'Found matches in {elapsed_time} sec.')

        res = {"matches": list(map(lambda key:
                                   {"image": utils.img_to_byte_str(person_matches[key]["path"]),
                                    "tag": person_matches[key]["tag"],
                                    "distance": key,
                                    "proximity": person_matches[key]["proximity"]},
                                   person_matches))}

        elapsed_time = round((datetime.datetime.now() - match_start_time).total_seconds(), t_acc)
        self.logger.info(f'Successfully matched in {elapsed_time} sec.')

        return utils.json_response(200, 'Success.', res)
