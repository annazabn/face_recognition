from src.http_handlers.upload import UploadsHTTPHandler
from src.http_handlers.match import MatchHTTPHandler
from src.http_handlers.compare import CompareHTTPHandler
from src.http_handlers.apikeys import APIKeysHTTPHandler
from src.http_handlers.healthcheck import Healthcheck