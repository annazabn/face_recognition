import cv2
import mxnet as mx
import numpy as np

from src.face_processor.arc_face_model import FaceModel
from src.face_processor.mtcnn_detector import MtcnnDetector
from src.config import CONFIG


paths = CONFIG['RESOURCES']

# create a detector, for run on gpu ctx=mx.gpu(0)
detector = MtcnnDetector(model_folder=paths['MTCNN_MODEL'],
                         threshold=[0.6, 0.8, 0.9],
                         ctx=mx.cpu(0), num_worker=1, accurate_landmark=True)

# init a get descriptor model
args = {'image_size': '112,112',
        'model': paths['ARCFACE_MODEL'],
        'det': 0, 'threshold': 1.24}
model = FaceModel(args)


def flask_lets_get_faces(img):
    # input img in rgb
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

    results = detector.detect_face(img)
    total_boxes = None
    chips = None

    if results is not None:

        total_boxes = results[0]
        points = results[1]

        #         print('points', points)
        # extract aligned face chips
        # extract aligned face chips
        chips = detector.extract_image_chips(img, points, 112, 0.37)
        # возвращает в бгр
        chips = list(map(lambda img: cv2.cvtColor(img, cv2.COLOR_BGR2RGB), chips))

    return chips, total_boxes


def get_embeddings(img):
    faces, boxes = flask_lets_get_faces(img)

    if faces is None:
        return None
    descriptors = []
    regions = []

    for i, face in enumerate(faces):
        # face = cv2.cvtColor(face, cv2.COLOR_RGB2BGR)
        face = np.transpose(face, (2, 0, 1))

        embds = model.get_feature(face)

        # append regions and descriptors
        descriptors.append(np.array(embds))
        regions.append(boxes[i])

    descs_and_points = list(map(lambda i: (descriptors[i], regions[i]), range(len(descriptors))))

    return descs_and_points
