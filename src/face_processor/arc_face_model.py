import mxnet as mx
import numpy as np
from sklearn import preprocessing


def get_model(ctx, image_size, model_str, layer):
    vec = model_str.split(',')
    assert len(vec) == 2
    prefix = vec[0]
    epoch = int(vec[1])
    print('loading', prefix, epoch)
    sym, arg_params, aux_params = mx.model.load_checkpoint(prefix, epoch)
    all_layers = sym.get_internals()
    sym = all_layers[layer + '_output']
    model = mx.mod.Module(symbol=sym, context=ctx, label_names=None)
    model.bind(data_shapes=[('data', (1, 3, image_size[0], image_size[1]))])
    model.set_params(arg_params, aux_params)
    return model


class FaceModel:
    def __init__(self, args):
        self.args = args
        ctx = mx.cpu()
        vec = args['image_size'].split(',')
        assert len(vec) == 2
        image_size = (int(vec[0]), int(vec[1]))
        self.model = None
        if len(args['model']) > 0:
            self.model = get_model(ctx, image_size, args['model'], 'fc1')

        self.threshold = args['threshold']
        self.image_size = image_size

    def get_feature(self, aligned):
        input_blob = np.expand_dims(aligned, axis=0)
        data = mx.nd.array(input_blob)
        db = mx.io.DataBatch(data=(data,))
        self.model.forward(db, is_train=False)
        embedding = self.model.get_outputs()[0].asnumpy()
        embedding = preprocessing.normalize(embedding).flatten()
        return embedding
