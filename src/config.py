CONFIG = {
    'APP': {
        'NAME': 'face_api',
        'PORT': 5000,
    },
    'RESOURCES': {
        'MTCNN_MODEL': 'src/resources/mtcnn_models/',
        'ARCFACE_MODEL': 'src/resources/arc_face_model/model-r50-am-lfw, 15',
        'CONFIDENCE_MODEL': 'src/resources/confidence_model.npy'
    }
}
