import os

from argparse import ArgumentParser
from enum import Enum
from pymongo import MongoClient

from src.resources import tokens


class ServerType(Enum):
    prod = 'prod'
    uat = 'uat'

    def __str__(self):
        return self.value


db_host = os.getenv('MONGO_HOST', 'localhost')
db_port = int(os.getenv('MONGO_PORT', 27017))
print(f'Connecting to {db_host}, {db_port}...')

client = MongoClient(db_host, db_port)
db = client.face_db
userdata = db.userdata

parser = ArgumentParser()
parser.add_argument('--mode', type=ServerType, choices=list(ServerType), default=ServerType.prod)
args = parser.parse_args()
current_server = args.mode
token_list = tokens.prod if current_server == ServerType.prod else tokens.uat

for token in token_list:
    userdata.update({'api_key': token}, {"&set": {'api_key': token}}, upsert=True)
print(f'Wrote {len(token_list)} tokens to {current_server}.')
