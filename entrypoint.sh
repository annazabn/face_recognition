#!/usr/bin/env bash
set -e

CMD="$1"

case "$CMD" in
gunicorn) shift; gunicorn -w 2 -b :$GUNICORN_PORT src.app:app $@;;
*) exec "$@";;

esac
